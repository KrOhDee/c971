﻿using app.Data;
using app.Models;
using System;

namespace app;

public partial class AssessmentEntryPage : ContentPage
{
    //Encapsulation
    private Course _course;
    private DatabaseService _dbService;
    private Assessment _assessment;

    public AssessmentEntryPage(Course course, Assessment assessment = null)
    {
        InitializeComponent();
        _course = course;
        _assessment = assessment;
        _dbService = new DatabaseService();
        AssessmentTypePicker.ItemsSource = new List<string> { "Performance", "Objective" };

        if (_assessment != null)
        {
            AssessmentNameEntry.Text = _assessment.Name;
            AssessmentStartDatePicker.Date = _assessment.StartDate;
            AssessmentEndDatePicker.Date = _assessment.EndDate;
            AssessmentTypePicker.SelectedItem = _assessment.Type;
        }
    }

    private async void OnSaveAssessmentClicked(object sender, EventArgs e)
    {
        var startDate = CombineDateAndTime(AssessmentStartDatePicker.Date, AssessmentStartTimePicker.Time);
        var endDate = CombineDateAndTime(AssessmentEndDatePicker.Date, AssessmentEndTimePicker.Time);

        if (string.IsNullOrWhiteSpace(AssessmentNameEntry.Text))
        {
            await DisplayAlert("Validation Error", "Assessment name is required.", "OK");
            return;
        }

        if (AssessmentTypePicker.SelectedItem == null)
        {
            await DisplayAlert("Validation Error", "Assessment type is required.", "OK");
            return;
        }

        if (startDate > endDate)
        {
            await DisplayAlert("Validation Error", "Start date cannot be after the end date.", "OK");
            return;
        }

        var existingAssessments = await _dbService.GetAssessmentsAsync(_course.Id);
        bool isDuplicateType = existingAssessments.Any(a => a.Type == AssessmentTypePicker.SelectedItem.ToString() && a.Id != (_assessment?.Id ?? 0));

        if (isDuplicateType)
        {
            await DisplayAlert("Error", "Only one PA and one OA can be added per course.", "OK");
            return;
        }

        int notificationTimeBeforeStart = 0;
        if (NotificationToggle.IsToggled)
        {
            if (NotificationTimePicker.SelectedItem == null)
            {
                await DisplayAlert("Selection Required", "Please select a notification time.", "OK");
                return;
            }
            notificationTimeBeforeStart = ConvertTimeToMinutes(NotificationTimePicker.SelectedItem.ToString());
        }

        var assessmentToSave = _assessment ?? new Assessment();
        assessmentToSave.Name = AssessmentNameEntry.Text;
        assessmentToSave.StartDate = startDate;
        assessmentToSave.EndDate = endDate;
        assessmentToSave.Type = AssessmentTypePicker.SelectedItem.ToString();
        assessmentToSave.CourseId = _course.Id;
        assessmentToSave.IsNotificationEnabled = NotificationToggle.IsToggled;
        assessmentToSave.NotificationTimeBeforeStart = notificationTimeBeforeStart;

        if (_assessment != null)
        {
            await _dbService.UpdateAssessmentAsync(assessmentToSave);
        }
        else
        {
            await _dbService.AddAssessmentAsync(assessmentToSave);
        }

        await DisplayAlert("Success", "Assessment saved successfully.", "OK");
        await Navigation.PopAsync();
    }

    private DateTime CombineDateAndTime(DateTime date, TimeSpan time)
    {
        return new DateTime(date.Year, date.Month, date.Day, time.Hours, time.Minutes, 0);
    }

    private int ConvertTimeToMinutes(string time)
    {
        switch (time)
        {
            case "30 Minutes":
                return 30;
            case "1 Hour":
                return 60;
            case "2 Hours":
                return 120;
            case "1 Day":
                return 1440;
            default:
                return 0;
        }
    }
}

