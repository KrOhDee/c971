﻿namespace app;
using app.Models;
using app.Data;
using static Android.Graphics.ImageDecoder;
using static Android.Util.EventLogTags;

public partial class CourseEntryPage : ContentPage
{
    public Course Course { get; set; }

    public CourseEntryPage(Course course = null)
    {
        InitializeComponent();
        Course = course ?? new Course { StartDate = DateTime.Today, EndDate = DateTime.Today };
        BindData();
        PopulateStatusPicker();
    }

    private bool IsValidEmail(string email)
    {
        if (string.IsNullOrWhiteSpace(email))
            return false;

        try
        {
            email = System.Text.RegularExpressions.Regex.Replace(email, @"(@)(.+)$", DomainMapper,
                                  System.Text.RegularExpressions.RegexOptions.None, TimeSpan.FromMilliseconds(200));

            string DomainMapper(System.Text.RegularExpressions.Match match)
            {
                var idn = new System.Globalization.IdnMapping();
                var domainName = idn.GetAscii(match.Groups[2].Value);
                return match.Groups[1].Value + domainName;
            }
        }
        catch (System.Text.RegularExpressions.RegexMatchTimeoutException e)
        {
            return false;
        }
        catch (ArgumentException e)
        {
            return false;
        }

        try
        {
            return System.Text.RegularExpressions.Regex.IsMatch(email,
                @"^[^@\s]+@[^@\s]+\.[^@\s]+$",
                System.Text.RegularExpressions.RegexOptions.IgnoreCase, TimeSpan.FromMilliseconds(250));
        }
        catch (System.Text.RegularExpressions.RegexMatchTimeoutException)
        {
            return false;
        }
    }

    private void PopulateStatusPicker()
    {
        StatusPicker.Items.Clear();

        var statuses = new List<string> { "In Progress", "Completed", "Planned", "Dropped" };
        foreach (var status in statuses)
        {
            StatusPicker.Items.Add(status);
        }

        StatusPicker.SelectedItem = Course.Status ?? statuses.First();
    }

    private void BindData()
    {
        CourseNameEntry.Text = Course.Name;
        StartDatePicker.MinimumDate = new DateTime(2000, 1, 1);
        EndDatePicker.MinimumDate = new DateTime(2000, 1, 1);
        InstructorNameEntry.Text = Course.InstructorName;
        InstructorPhoneEntry.Text = Course.InstructorPhone;
        InstructorEmailEntry.Text = Course.InstructorEmail;
        NotesEditor.Text = Course.Notes;
    }

    private async void OnSaveCourseClicked(object sender, EventArgs e)
    {
        var startDate = CombineDateAndTime(StartDatePicker.Date, StartTimePicker.Time);
        var endDate = CombineDateAndTime(EndDatePicker.Date, EndTimePicker.Time);

        if (string.IsNullOrWhiteSpace(CourseNameEntry.Text))
        {
            await DisplayAlert("Error", "Course name is required.", "OK");
            return;
        }

        if (StatusPicker.SelectedItem == null)
        {
            await DisplayAlert("Selection Required", "Please select a course status.", "OK");
            return;
        }

        if (!IsValidEmail(InstructorEmailEntry.Text))
        {
            await DisplayAlert("Error", "Please enter a valid email address.", "OK");
            return;
        }

        if (startDate > endDate)
        {
            await DisplayAlert("Validation Error", "Start date cannot be after the end date.", "OK");
            return;
        }

        if (NotificationToggle.IsToggled)
        {
            if (NotificationTimePicker.SelectedItem == null)
            {
                await DisplayAlert("Selection Required", "Please select a notification time.", "OK");
                return;
            }

            Course.NotificationTimeBeforeStart = ConvertTimeToMinutes(NotificationTimePicker.SelectedItem.ToString());
        }
        else
        {
            Course.NotificationTimeBeforeStart = 0;
        }

        Course.Name = CourseNameEntry.Text;
        Course.StartDate = startDate;
        Course.EndDate = endDate;
        Course.Status = StatusPicker.SelectedItem.ToString();
        Course.InstructorName = InstructorNameEntry.Text;
        Course.InstructorPhone = InstructorPhoneEntry.Text;
        Course.InstructorEmail = InstructorEmailEntry.Text;
        Course.Notes = NotesEditor.Text;
        Course.IsNotificationEnabled = NotificationToggle.IsToggled;

        var dbService = new DatabaseService();

        if (Course.Id == 0)
        {
            await dbService.AddCourseAsync(Course);
        }
        else
        {
            await dbService.UpdateCourseAsync(Course);
        }

        await Navigation.PopModalAsync();
    }


    private DateTime CombineDateAndTime(DateTime date, TimeSpan time)
    {
        return new DateTime(date.Year, date.Month, date.Day, time.Hours, time.Minutes, 0);
    }

    private int ConvertTimeToMinutes(string time)
    {
        switch (time)
        {
            case "30 Minutes":
                return 30;
            case "1 Hour":
                return 60;
            case "2 Hours":
                return 120;
            case "1 Day":
                return 1440;
            default:
                return 0;
        }
    }

    private async void OnCancelClicked(object sender, EventArgs e)
    {
        await Navigation.PopModalAsync();
    }
}

