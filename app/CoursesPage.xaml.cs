﻿using System.Collections.ObjectModel;
using app.Models;
using app.Data;
using Android.Telecom;

namespace app;

public partial class CoursesPage : ContentPage
{
    private Term _selectedTerm;
    public Term SelectedTerm
    {
        get => _selectedTerm;
        set
        {
            _selectedTerm = value;
            OnPropertyChanged(nameof(SelectedTerm));
        }
    }

    public ObservableCollection<Course> Courses { get; set; }
    public CoursesPage(Term term)
    {
        InitializeComponent();
        SelectedTerm = term;
        this.BindingContext = this;
        Courses = new ObservableCollection<Course>();
        CoursesList.ItemsSource = Courses;
        Task.Run(async () => await LoadCourses());
    }

    private async Task LoadCourses()
    {
        var dbService = new DatabaseService();
        var termCourses = await dbService.GetCoursesAsync(SelectedTerm.Id);
        Courses.Clear();
        foreach (var course in termCourses)
        {
            Courses.Add(course);
        }
    }

    private async void OnAddCourseClicked(object sender, EventArgs e)
    {
        if (Courses.Count >= 6)
        {
            await DisplayAlert("Limit Reached", "A maximum of 6 courses can be added to a term.", "OK");
            return;
        }

        var newCourse = new Course { TermId = SelectedTerm.Id };
        var courseEntryPage = new CourseEntryPage(newCourse);
        courseEntryPage.Disappearing += OnCourseEntryPageClosed;
        await Navigation.PushModalAsync(courseEntryPage);
    }

    private async void OnEditCourseClicked(object sender, EventArgs e)
    {
        var button = sender as Button;
        var course = button.BindingContext as Course;
        var courseEntryPage = new CourseEntryPage(course);
        courseEntryPage.Disappearing += OnCourseEntryPageClosed;
        await Navigation.PushModalAsync(courseEntryPage);
    }

    private async void OnDeleteCourseClicked(object sender, EventArgs e)
    {
        var button = sender as Button;
        var course = button.BindingContext as Course;
        bool confirm = await DisplayAlert("Confirm", "Are you sure you want to delete this course?", "Yes", "No");
        if (confirm)
        {
            var dbService = new DatabaseService();
            await dbService.DeleteCourseAsync(course);
            await LoadCourses();
        }
    }

    private async void OnAddAssessmentClicked(object sender, EventArgs e)
    {
        var button = (Button)sender;
        var course = (Course)button.BindingContext;
        await Navigation.PushAsync(new AssessmentEntryPage(course));
    }

    private async void OnViewCourseClicked(object sender, EventArgs e)
    {
        var button = sender as Button;
        var course = button.CommandParameter as Course;

        await Navigation.PushAsync(new DetailedCoursePage(course));
    }

    private async void OnCourseEntryPageClosed(object sender, EventArgs e)
    {
        ((ContentPage)sender).Disappearing -= OnCourseEntryPageClosed;
        await LoadCourses();
    }

}


