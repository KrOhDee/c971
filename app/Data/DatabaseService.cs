﻿using SQLite;
using System;
using System.IO;
using System.Threading.Tasks;
using app.Models;
using System.Collections.Generic;

namespace app.Data
{

    public class DatabaseService
    {
        private SQLiteAsyncConnection _database;

        public DatabaseService()
        {
            var dbPath = GetDatabasePath();
            _database = new SQLiteAsyncConnection(dbPath);

            _database.CreateTableAsync<Term>().Wait();
            _database.CreateTableAsync<Course>().Wait();
            _database.CreateTableAsync<Assessment>().Wait();
        }

        private string GetDatabasePath()
        {
            var folderPath = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
            return Path.Combine(folderPath, "database1.db");
        }

        public Task<int> AddTermAsync(Term term) => _database.InsertAsync(term);
        public Task<int> UpdateTermAsync(Term term) => _database.UpdateAsync(term);
        public async Task<int> DeleteTermAsync(Term term)
        {
            var courses = await _database.Table<Course>().Where(c => c.TermId == term.Id).ToListAsync();
            foreach (var course in courses)
            {
                var assessments = await _database.Table<Assessment>().Where(a => a.CourseId == course.Id).ToListAsync();
                foreach (var assessment in assessments)
                {
                    await _database.DeleteAsync(assessment);
                }
                await _database.DeleteAsync(course);
            }
            return await _database.DeleteAsync(term);
        }
        public Task<Term> GetTermAsync(int id) => _database.Table<Term>().Where(x => x.Id == id).FirstOrDefaultAsync();
        public Task<List<Term>> GetTermsAsync() => _database.Table<Term>().ToListAsync();

        public Task<int> AddCourseAsync(Course course) => _database.InsertAsync(course);
        public Task<int> UpdateCourseAsync(Course course) => _database.UpdateAsync(course);
        public async Task<int> DeleteCourseAsync(Course course)
        {
            var assessments = await _database.Table<Assessment>().Where(a => a.CourseId == course.Id).ToListAsync();
            foreach (var assessment in assessments)
            {
                await _database.DeleteAsync(assessment);
            }
            return await _database.DeleteAsync(course);
        }
        public Task<Course> GetCourseAsync(int id) => _database.Table<Course>().Where(x => x.Id == id).FirstOrDefaultAsync();
        public Task<List<Course>> GetCoursesAsync(int termId)
        {
            return _database.Table<Course>().Where(c => c.TermId == termId).ToListAsync();
        }
        public async Task<List<Course>> GetCoursesStartingToday()
        {
            var today = DateTime.Today;
            var allCourses = await _database.Table<Course>().ToListAsync();
            return allCourses.Where(c => c.StartDate.Date == today).ToList();
        }


        public Task<int> AddAssessmentAsync(Assessment assessment) => _database.InsertAsync(assessment);
        public Task<int> UpdateAssessmentAsync(Assessment assessment) => _database.UpdateAsync(assessment);
        public Task<int> DeleteAssessmentAsync(Assessment assessment) => _database.DeleteAsync(assessment);
        public Task<Assessment> GetAssessmentAsync(int id) => _database.Table<Assessment>().Where(x => x.Id == id).FirstOrDefaultAsync();
        public Task<List<Assessment>> GetAssessmentsAsync(int courseId)
        {
            return _database.Table<Assessment>().Where(a => a.CourseId == courseId).ToListAsync();
        }
        public async Task<List<Course>> GetNotifiableCoursesAsync()
        {
            var allCourses = await _database.Table<Course>().ToListAsync();
            return allCourses.Where(c => c.IsNotificationEnabled).ToList();
        }
        public async Task<List<Assessment>> GetNotifiableAssessmentsAsync()
        {
            var allAssessments = await _database.Table<Assessment>().ToListAsync();
            return allAssessments.Where(a => a.IsNotificationEnabled).ToList();
        }
        public Task<int> DeleteAllAssessmentsAsync()
        {
            return _database.DeleteAllAsync<Assessment>();
        }

        public async Task<List<ReportRowModel>> GetReportDataAsync()
        {
            var reportData = new List<ReportRowModel>();
            var courses = await _database.Table<Course>().ToListAsync();

            foreach (var course in courses)
            {
                var term = await _database.Table<Term>().Where(t => t.Id == course.TermId).FirstOrDefaultAsync();
                var reportRow = new ReportRowModel
                {
                    CourseName = course.Name,
                    TermName = term?.Title,
                    Status = course.Status,
                    InstructorName = course.InstructorName,
                    StartDate = course.StartDate,
                    EndDate = course.EndDate
                };
                reportData.Add(reportRow);
            }
            return reportData;
        }
    }
}