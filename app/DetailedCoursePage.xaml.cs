﻿using System.Collections.ObjectModel;
using app.Data;
using app.Models;
using Microsoft.Maui.ApplicationModel;

namespace app;

public partial class DetailedCoursePage : ContentPage
{
    private Course _course;
    private DatabaseService _dbService;
    public ObservableCollection<Assessment> Assessments { get; set; }

    public DetailedCoursePage(Course course)
    {
        InitializeComponent();
        _course = course;
        _dbService = new DatabaseService();
        Assessments = new ObservableCollection<Assessment>();
        AssessmentsList.ItemsSource = Assessments;
        Task.Run(async () => await BindCourseDetails());
    }

    private async Task BindCourseDetails()
    {
        CourseNameLabel.Text = _course.Name;
        StartDateLabel.Text = $"Start: {_course.StartDate:d}";
        EndDateLabel.Text = $"End: {_course.EndDate:d}";
        NotesLabel.Text = $"Notes: {_course.Notes}";
        InstructorNameLabel.Text = $"Instructor: {_course.InstructorName}";
        InstructorPhoneLabel.Text = $"Phone: {_course.InstructorPhone}";
        InstructorEmailLabel.Text = $"Email: {_course.InstructorEmail}";
        CourseStatusLabel.Text = $"Status: {_course.Status}";

        var assessments = await _dbService.GetAssessmentsAsync(_course.Id);
        Assessments.Clear();
        foreach (var assessment in assessments)
        {
            Assessments.Add(assessment);
        }
    }

    private async void OnEditAssessmentClicked(object sender, EventArgs e)
    {
        var button = (Button)sender;
        var assessment = (Assessment)button.CommandParameter;
        var assessmentPage = new AssessmentEntryPage(_course, assessment);
        assessmentPage.Disappearing += AssessmentPage_Disappearing;
        await Navigation.PushAsync(assessmentPage);
    }

    private async void OnDeleteAssessmentClicked(object sender, EventArgs e)
    {
        var button = (Button)sender;
        var assessment = (Assessment)button.CommandParameter;

        bool confirm = await DisplayAlert("Confirm", "Are you sure you want to delete this assessment?", "Yes", "No");
        if (confirm)
        {
            await _dbService.DeleteAssessmentAsync(assessment);
            await BindCourseDetails();
        }
    }

    private async void OnShareNotesClicked(object sender, EventArgs e)
    {
        if (string.IsNullOrWhiteSpace(_course.Notes))
        {
            await DisplayAlert("Info", "There are no notes to share for this course.", "OK");
            return;
        }

        try
        {
            await Share.RequestAsync(new ShareTextRequest
            {
                Title = "Share Course Notes",
                Text = _course.Notes,
                Subject = $"Notes for {_course.Name}"
            });
        }
        catch (Exception ex)
        {
            await DisplayAlert("Error", "Unable to share notes at this time.", "OK");
        }
    }

    private async void AssessmentPage_Disappearing(object sender, EventArgs e)
    {
        ((AssessmentEntryPage)sender).Disappearing -= AssessmentPage_Disappearing;
        await BindCourseDetails(); 
    }
}
