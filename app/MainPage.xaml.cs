﻿using System;
using System.Collections.ObjectModel;
using app.Models;
using app.Data;
using Android.App.Job;
using Plugin.LocalNotification;

namespace app;

// Completed design document including class diagram and design diagram
// Completed user guide for maintenance and user guide for running app
// from user perspective
// Completed test plan with unit test scripts, results, summary of changes
// and screenshots all included
// Finished panopto video
// All reqs for part A, B, C, D, E done

public partial class MainPage : ContentPage
{
    ObservableCollection<Term> terms;
    private DatabaseService dbService;

    public MainPage()
    {
        InitializeComponent();
        terms = new ObservableCollection<Term>();
        TermsList.ItemsSource = terms;
        dbService = new DatabaseService();
        //Task.Run(async () => await dbService.DeleteAllAssessmentsAsync());
        CheckAndNotifyForTodayCoursesAndAssessments();
    }

    protected override async void OnAppearing()
    {
        base.OnAppearing();
        await InitializeDataAsync();
        StartNotificationCheckTimer();
    }

    private async Task InitializeDataAsync()
    {
        await AddDefaultDataIfNeeded();
        await FetchTerms();
    }

    private void StartNotificationCheckTimer()
    {
        Device.StartTimer(TimeSpan.FromMinutes(1), () =>
        {
            CheckAndNotifyForUpcomingCoursesAndAssessments();
            return true;
        });
    }

    private async void CheckAndNotifyForUpcomingCoursesAndAssessments()
    {
        var notifiableCourses = await dbService.GetNotifiableCoursesAsync();
        var notifiableAssessments = await dbService.GetNotifiableAssessmentsAsync();

        foreach (var course in notifiableCourses)
        {
            ScheduleCourseNotification(course);
        }

        foreach (var assessment in notifiableAssessments)
        {
            ScheduleAssessmentNotification(assessment);
        }
    }

    private async Task AddDefaultDataIfNeeded()
    {
        var existingTerms = await dbService.GetTermsAsync();
        if (existingTerms.Any())
        {
            return;
        }

        var term = new Term
        {
            Title = "Default Term",
            StartDate = new DateTime(2023, 1, 1),
            EndDate = new DateTime(2023, 12, 31)
        };
        await dbService.AddTermAsync(term);

        var course = new Course
        {
            Name = "Default Course",
            StartDate = new DateTime(2023, 1, 1),
            EndDate = new DateTime(2023, 6, 30),
            InstructorName = "Anika Patel",
            InstructorPhone = "555-123-4567",
            InstructorEmail = "anika.patel@strimeuniversity.edu",
            Status = "Planned",
            Notes = "Notes for the course",
            TermId = term.Id 
        };
        await dbService.AddCourseAsync(course);

        var assessment1 = new Assessment
        {
            Name = "PA - Default",
            StartDate = new DateTime(2023, 1, 15),
            EndDate = new DateTime(2023, 1, 31),
            Type = "Performance",
            CourseId = course.Id
        };
        var assessment2 = new Assessment
        {
            Name = "OA - Default",
            StartDate = new DateTime(2023, 2, 1),
            EndDate = new DateTime(2023, 2, 15),
            Type = "Objective",
            CourseId = course.Id
        };
        await dbService.AddAssessmentAsync(assessment1);
        await dbService.AddAssessmentAsync(assessment2);
    }

    private async void CheckAndNotifyForTodayCoursesAndAssessments()
    {
        var notifiableCourses = await dbService.GetNotifiableCoursesAsync();
        var notifiableAssessments = await dbService.GetNotifiableAssessmentsAsync();

        foreach (var course in notifiableCourses)
        {
            ScheduleCourseNotification(course);
        }

        foreach (var assessment in notifiableAssessments)
        {
            ScheduleAssessmentNotification(assessment);
        }
    }

    private void ScheduleCourseNotification(Course course)
    {
        var notifyTime = course.StartDate.AddMinutes(-course.NotificationTimeBeforeStart);
        var currentTime = DateTime.Now;

        if (currentTime >= notifyTime.AddSeconds(-60) && currentTime < notifyTime)
        {
            var notification = new NotificationRequest
            {
                NotificationId = new Random().Next(),
                Title = $"{course.Name} is starting soon!",
                Description = "Reminder: Your course is about to start.",
                Schedule = new NotificationRequestSchedule { NotifyTime = notifyTime }
            };
            LocalNotificationCenter.Current.Show(notification);
        }
    }

    private void ScheduleAssessmentNotification(Assessment assessment)
    {
        var notifyTime = assessment.StartDate.AddMinutes(-assessment.NotificationTimeBeforeStart);
        var currentTime = DateTime.Now;

        if (currentTime >= notifyTime.AddSeconds(-60) && currentTime < notifyTime)
        {
            var notification = new NotificationRequest
            {
                NotificationId = new Random().Next(),
                Title = $"{assessment.Name} is starting soon!",
                Description = "Reminder: Your assessment is about to start.",
                Schedule = new NotificationRequestSchedule { NotifyTime = notifyTime }
            };
            LocalNotificationCenter.Current.Show(notification);
        }
    }

    private async void OnAddTermClicked(object sender, EventArgs e)
    {
        var termEntryPage = new TermEntryPage();
        termEntryPage.Disappearing += OnTermEntryPageClosed; 
        await Navigation.PushModalAsync(termEntryPage);
    }

    private async void OnEditTermClicked(object sender, EventArgs e)
    {
        var term = (sender as Button).BindingContext as Term;
        var termEntryPage = new TermEntryPage(term);
        termEntryPage.Disappearing += OnTermEntryPageClosed; 
        await Navigation.PushModalAsync(termEntryPage);
    }

    private async void OnDeleteTermClicked(object sender, EventArgs e)
    {
        var term = (sender as Button).BindingContext as Term;
        bool confirm = await DisplayAlert("Confirm", "Are you sure you want to delete this term?", "Yes", "No");
        if (confirm)
        {
            var dbService = new DatabaseService();
            await dbService.DeleteTermAsync(term);
            await FetchTerms();
        }
    }

    private void OnSearchButtonPressed(object sender, EventArgs e)
    {
        string query = SearchBar.Text.ToLower();
        var filteredTerms = terms.Where(t => t.Title.ToLower().Contains(query)).ToList();
        terms.Clear();
        foreach (var term in filteredTerms)
        {
            terms.Add(term);
        }
    }

    private async void OnClearButtonPressed(object sender, EventArgs e)
    {
        SearchBar.Text = string.Empty;
        await FetchTerms();
    }

    private async void OnViewCoursesClicked(object sender, EventArgs e)
    {
        var term = (sender as Button).BindingContext as Term;
        await Navigation.PushAsync(new CoursesPage(term));
    }

    private async void OnGenerateReportClicked(object sender, EventArgs e)
    {
        await Navigation.PushAsync(new ReportPage());
    }

    private async Task FetchTerms()
    {
        var dbService = new DatabaseService();
        var allTerms = await dbService.GetTermsAsync();
        terms.Clear();
        foreach (var term in allTerms)
        {
            terms.Add(term);
        }
    }

    private async void OnTermEntryPageClosed(object sender, EventArgs e)
    {
        ((ContentPage)sender).Disappearing -= OnTermEntryPageClosed; 
        await FetchTerms(); 
    }
}


