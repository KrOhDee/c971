﻿using System;
using SQLite;
using System.Collections.Generic;

namespace app.Models
{
    public class Assessment : BaseEntity, IPrintable
    {
        public string Name { get; set; }
        public string Type { get; set; }
        public bool IsNotificationEnabled { get; set; }
        public int NotificationTimeBeforeStart { get; set; }
        public int CourseId { get; set; }

        public void PrintDetails()
        {
            Console.WriteLine("Assessment Details:");
            Console.WriteLine($"Name: {Name}");
            Console.WriteLine($"Type: {Type}");
            Console.WriteLine($"Is Notification Enabled: {IsNotificationEnabled}");
            Console.WriteLine($"Notification Time Before Start (minutes): {NotificationTimeBeforeStart}");
            Console.WriteLine($"Course Id: {CourseId}");
        }
    }
}

