﻿using System;
using SQLite;
using System.Collections.Generic;

namespace app.Models
{
    public class Course : BaseEntity, IPrintable
    {
        public string Name { get; set; }
        public string Status { get; set; }
        public string Notes { get; set; }

        public bool IsNotificationEnabled { get; set; }
        public int NotificationTimeBeforeStart { get; set; }

        public string InstructorName { get; set; }
        public string InstructorPhone { get; set; }
        public string InstructorEmail { get; set; }

        public int TermId { get; set; }

        [Ignore]
        public List<Assessment> Assessments { get; set; }

        public void PrintDetails()
        {
            Console.WriteLine("Course Details:");
            Console.WriteLine($"Name: {Name}");
            Console.WriteLine($"Status: {Status}");
            Console.WriteLine($"Notes: {Notes}");
            Console.WriteLine($"Is Notification Enabled: {IsNotificationEnabled}");
            Console.WriteLine($"Notification Time Before Start (minutes): {NotificationTimeBeforeStart}");
            Console.WriteLine($"Instructor Name: {InstructorName}");
            Console.WriteLine($"Instructor Phone: {InstructorPhone}");
            Console.WriteLine($"Instructor Email: {InstructorEmail}");
            Console.WriteLine($"Term Id: {TermId}");
        }
    }
}

