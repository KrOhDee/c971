﻿using System;
namespace app.Models
{
    //Polymorphism: The purpose of this is for debugging.
	public interface IPrintable
	{
        void PrintDetails();
    }
}

