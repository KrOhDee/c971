﻿using System;
namespace app.Models
{
    public class ReportRowModel
    {
        public string CourseName { get; set; }
        public string TermName { get; set; }
        public string Status { get; set; }
        public string InstructorName { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }
}

