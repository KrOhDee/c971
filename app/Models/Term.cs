﻿using System;
using SQLite;
using System.Collections.Generic;
using static Android.Graphics.ImageDecoder;

namespace app.Models
{
    public class Term : BaseEntity, IPrintable
    {
        public string Title { get; set; }

        [Ignore]
        public List<Course> Courses { get; set; }

        public void PrintDetails()
        {
            Console.WriteLine("Term Details:");
            Console.WriteLine($"Title: {Title}");
            Console.WriteLine($"Courses: {Courses?.Count ?? 0}");
        }
    }
}

