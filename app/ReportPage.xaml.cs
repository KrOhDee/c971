﻿using System.Collections.ObjectModel;
using app.Models;
using app.Data;

namespace app;

public partial class ReportPage : ContentPage
{
    public ObservableCollection<ReportRowModel> ReportData { get; set; }

    public ReportPage()
    {
        InitializeComponent();
        ReportData = new ObservableCollection<ReportRowModel>();
        Task.Run(async () => await FetchAndGenerateReport());
        TimestampLabel.Text = $"Report generated on: {DateTime.Now}";
        BindingContext = this;
    }

    private async Task FetchAndGenerateReport()
    {
        var dbService = new DatabaseService();
        var reportData = await dbService.GetReportDataAsync();

        foreach (var row in reportData)
        {
            ReportData.Add(row);
        }
    }
}
