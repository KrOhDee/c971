﻿using app.Models;
using app.Data;

namespace app;

public partial class TermEntryPage : ContentPage
{
    public Term Term { get; set; }

    public TermEntryPage(Term term = null)
    {
        InitializeComponent();
        Term = term ?? new Term { StartDate = DateTime.Today, EndDate = DateTime.Today };
        BindData();
    }

    private void BindData()
    {
        TermTitleEntry.Text = Term.Title;
        StartDatePicker.Date = Term.StartDate;
        EndDatePicker.Date = Term.EndDate;
    }

    private async void OnSaveTermClicked(object sender, EventArgs e)
    {
        if (string.IsNullOrWhiteSpace(TermTitleEntry.Text))
        {
            await DisplayAlert("Error", "Term title is required.", "OK");
            return;
        }

        if (StartDatePicker.Date > EndDatePicker.Date)
        {
            await DisplayAlert("Validation Error", "Start date cannot be after the end date.", "OK");
            return;
        }

        Term.Title = TermTitleEntry.Text;
        Term.StartDate = StartDatePicker.Date;
        Term.EndDate = EndDatePicker.Date;

        var dbService = new DatabaseService();

        if (Term.Id == 0)
        {
            await dbService.AddTermAsync(Term);
        }
        else
        {
            await dbService.UpdateTermAsync(Term);
        }

        await Navigation.PopModalAsync();
    }

    private async void OnCancelClicked(object sender, EventArgs e)
    {
        await Navigation.PopModalAsync();
    }

}
